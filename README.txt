-------------------------------------------------------------------------------
--Black Swan for Drupal 7
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--About
-------------------------------------------------------------------------------
Black Swan is a minimalistic clean theme for Drupal 7.

-------------------------------------------------------------------------------
--Black Swan Features
-------------------------------------------------------------------------------
Addition to available Drupal theme features, Black Swan has the following
* Supports one/two column layout
* Based on Fluid 960.gs
* CSS3
* Google fonts
    http://www.google.com/webfonts/specimen/Orienta
    http://www.google.com/webfonts/specimen/Lustria
