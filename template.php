<?php

/**
 * Hooks for Black Swan 
 *
 */

/**
 * Implements hook_preprocess_node().
 */
function black_swan_preprocess_node(&$variables) {
  if ($variables['type'] == 'article' || $variables['type'] == 'blog') {
    $node = $variables['node'];
    $created_day = format_date($node->created, 'custom', 'j');
    $created_month = format_date($node->created, 'custom', 'M');
    $created_year = format_date($node->created, 'custom', 'Y');
    $variables['posted_on'] = $created_day . ' - ' . $created_month .' - ' . $created_year; 
  }
}

/**
 * Implements hook_preprocess_page().
 */
function black_swan_preprocess_page(&$variables) {
  $element = array(
    '#tag' => 'link',
    '#attributes' => array(
      'href' => 'http://fonts.googleapis.com/css?family=Orienta|Lustria',
      'rel' => 'stylesheet',
      'type' => 'text/css',
    ),
  );
  drupal_add_html_head($element, 'google_font_black_swan');
   if ($variables['page']['sidebar']) {
    $variables['sidebar_grid'] = 'grid_9';
  }
  else {
    $variables['sidebar_grid'] = 'grid_12';
  }
}



/**
 * Implements hook_preprocess_comment().
 */
function black_swan_preprocess_comment(&$variables) {
  $node = $variables['node'];
  $created_day = format_date($node->created, 'custom', 'j');
  $created_month = format_date($node->created, 'custom', 'M');
  $created_year = format_date($node->created, 'custom', 'Y');
  $variables['commented_on'] = $created_year . ' - ' . $created_month .' - ' . $created_day;
}

/**
 * Implements hook_preprocess_comment_wrapper().
 */
function black_swan_preprocess_comment_wrapper(&$vars) {
  $node = $vars['node'];
  $vars['commentcount'] = format_plural($node->comment_count, '1 comment', '@count comments');
}